﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T entity)
        {
            var guid = Guid.NewGuid();
            entity.Id = guid;
            Data = Data.Append(entity);
            return Task.FromResult(guid);
        }

        public Task EditAsync(Guid id, T entity)
        {
            Data = Data.Select(item => item.Id == id ? entity : item).ToList();
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            Data = Data.Where(x => x.Id != id);
            return Task.CompletedTask;
        }
    }
}