﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

public class AddEmployeeRequest
{
    public string FirstName { get; set; }
    public string LastName { get; set; }

    public string Email { get; set; }

    public List<Role> Roles { get; set; }
}