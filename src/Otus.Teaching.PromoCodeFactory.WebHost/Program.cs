using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Apis;


var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;

services.AddSingleton(typeof(IRepository<Employee>), (x) => 
    new InMemoryRepository<Employee>(FakeDataFactory.Employees));
services.AddSingleton(typeof(IRepository<Role>), (x) => 
     new InMemoryRepository<Role>(FakeDataFactory.Roles));

// services.AddScoped<IRepository<Employee>, InMemoryRepository<Employee>>();
// services.AddScoped<IRepository<Role>, InMemoryRepository<Role>>();

services.AddEndpointsApiExplorer();
services.AddOpenApiDocument(options =>
{
    options.Title = "PromoCode Factory API Doc";
    options.Version = "1.0";
});


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseHsts();
}

app.UseOpenApi();
app.UseSwaggerUi(x =>
{
    x.DocExpansion = "list";
});
            
app.UseHttpsRedirection();

app.UseRouting();

var apiVersion1 = app.MapGroup("/api/v1")
    .WithTags("API Version 1");

apiVersion1
    .MapGroup("/roles")
    .WithTags("Roles API")
    .MapRolesApi();

apiVersion1
    .MapGroup("/employees")
    .WithTags("Employees API")
    .MapEmployeesApi();

app.Run();
