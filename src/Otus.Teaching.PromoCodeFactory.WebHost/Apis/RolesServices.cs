﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Apis;

public class RolesServices(
    IRepository<Role> repository)
{
    public IRepository<Role> Repository { get; } = repository;
}