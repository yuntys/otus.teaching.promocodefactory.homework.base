﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Apis;

public static class EmployeesApi
{
    public static IEndpointRouteBuilder MapEmployeesApi(this IEndpointRouteBuilder app)
    {
        app.MapGet("/", GetEmployees);
        app.MapGet("/{id:guid}", GetEmployeeById);
        app.MapPost("/", AddEmployee);
        app.MapPut("/{id:guid}", EditEmployeeById);
        app.MapDelete("/{id:guid}", DeleteEmployeeById);

        return app;
    }
    
    /// <summary>
    /// Получить данные всех сотрудников
    /// </summary>
    /// <returns></returns>
    public static async Task<Ok<List<EmployeeShortResponse>>> GetEmployees(
        IRepository<Employee> employeeRepository)
    {
        var employees = await employeeRepository.GetAllAsync();

        var employeesModelList = employees.Select(x => 
            new EmployeeShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FullName = x.FullName,
            }).ToList();

        return TypedResults.Ok(employeesModelList);
    }
        
    /// <summary>
    /// Получить данные сотрудника по Id
    /// </summary>
    /// <returns></returns>
    public static async Task<Results<Ok<EmployeeResponse>, NotFound>> GetEmployeeById(
        IRepository<Employee> employeeRepository,
        Guid id)
    {
        var employee = await employeeRepository.GetByIdAsync(id);

        if (employee == null)
            return TypedResults.NotFound();
            
        var employeeModel = new EmployeeResponse()
        {
            Id = employee.Id,
            Email = employee.Email,
            Roles = employee.Roles.Select(x => new RoleItemResponse()
            {
                Name = x.Name,
                Description = x.Description
            }).ToList(),
            FullName = employee.FullName,
            AppliedPromocodesCount = employee.AppliedPromocodesCount
        };

        return TypedResults.Ok(employeeModel);
    }
    
    public static async Task<Results<Ok<Guid>,BadRequest>> AddEmployee(
        IRepository<Employee> employeeRepository,
        AddEmployeeRequest employeeToAdd)
    {
        var employee = new Employee()
        {
            FirstName = employeeToAdd.FirstName,
            LastName = employeeToAdd.LastName,
            Email = employeeToAdd.Email,
            Roles = employeeToAdd.Roles,
            AppliedPromocodesCount = 0
        };
        var addedEmployeeId = await employeeRepository.AddAsync(employee);

        return TypedResults.Ok(addedEmployeeId);
    }
    
    public static async Task<Ok> EditEmployeeById(
        IRepository<Employee> employeeRepository,
        Guid id,
        EditEmployeeRequest employeeToEdit)
    {
        var employee = new Employee()
        {
            FirstName = employeeToEdit.FirstName,
            LastName = employeeToEdit.LastName,
            Email = employeeToEdit.Email,
            Roles = employeeToEdit.Roles,
            AppliedPromocodesCount = employeeToEdit.AppliedPromocodesCount
        };
        await employeeRepository.EditAsync(id, employee);

        return TypedResults.Ok();
    }
    
    public static async Task<Ok> DeleteEmployeeById(
        IRepository<Employee> employeeRepository,
        Guid id)
    {
        await employeeRepository.DeleteAsync(id);

        return TypedResults.Ok();
    }
}