﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Routing;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Apis;

public static class RolesApi
{
    public static IEndpointRouteBuilder MapRolesApi(this IEndpointRouteBuilder app)
    {
        // Routes for querying roles
        app.MapGet("/", GetRoles);

        return app;
    }
    
    /// <summary>
    /// Получить все доступные роли сотрудников
    /// </summary>
    /// <returns></returns>
    public static async Task<Ok<List<RoleItemResponse>>> GetRoles(
        [AsParameters] RolesServices services)
    {
        var roles = await services.Repository.GetAllAsync();

        var rolesModelList = roles.Select(x => 
            new RoleItemResponse()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description
            }).ToList();

        return TypedResults.Ok(rolesModelList);
    }
}
